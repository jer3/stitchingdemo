#include <chrono>
#include <iostream>
#include <limits>

#include "opencv2\core.hpp"
#include "opencv2\calib3d.hpp"
#include "opencv2\features2d.hpp"
#include "opencv2\imgcodecs.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\imgproc.hpp"

cv::Size calculate_panorama_size(const cv::Mat& left_img, const cv::Mat& right_img, const cv::Mat& homography_matrix)
{
	const auto width = right_img.cols;
	const auto height = right_img.rows;

	// Find the corner coordinates for the transformed image after transforming
	// Note: warpPerspective does not return these values.
	std::vector<cv::Point2f> orig_corners(4);
	orig_corners[0] = cv::Point(0, 0);
	orig_corners[1] = cv::Point(width, 0);
	orig_corners[2] = cv::Point(0, height);
	orig_corners[3] = cv::Point(width, height);
	std::vector<cv::Point2f> transformed_corners(4);

	// Transform corner coordinates using homography matrix to obtain bounding box dimensions
	cv::perspectiveTransform(orig_corners, transformed_corners, homography_matrix);

	auto max_height = std::numeric_limits<float>::min();
	auto max_width = std::numeric_limits<float>::min();

	for each (const auto& point in transformed_corners)
	{
		if (point.y > max_height) { max_height = point.y; }
		if (point.x > max_width) { max_width = point.x; }
	}

	// Transform the maximum width and height to integer values
	const auto left_img_width = left_img.cols;
	const auto left_img_height = left_img.rows;

	int panorama_height = left_img_height;
	int panorama_width = left_img_width;

	if (max_height > left_img_height)	{ panorama_height = static_cast<int>(std::ceilf(max_height)); }
	if (max_width > left_img_width)		{ panorama_width = static_cast<int>(std::ceilf(max_width)); }
	
	return cv::Size(panorama_width, panorama_height);
}

std::vector<cv::DMatch> find_matching_descriptors(const cv::Mat& descriptors_left, const cv::Mat& descriptors_right)
{
	cv::FlannBasedMatcher matcher;
	std::vector<cv::DMatch> matches;
	matcher.match(descriptors_left, descriptors_right, matches);

	double min_dist = 100;

	// Find minimum distance between matching keypoints
	for each (const auto& match in matches)
	{
		double dist = match.distance;
		if (dist < min_dist) min_dist = dist;
	}

	// Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
	// or a small arbitary value ( 0.02 ) in the event that min_dist is very small)
	std::vector<cv::DMatch> good_matches;
	for each (const auto& match in matches)
	{
		if (match.distance <= std::max(2 * min_dist, 0.02))
		{
			good_matches.push_back(match);
		}
	}

	return good_matches;
}

cv::Mat create_panorama(const cv::Mat& img_left, const cv::Mat& warped_img, const cv::Size& panorama_size)
{
	// Create a "blank canvas" for panorama
	auto panorama = cv::Mat(panorama_size, img_left.type(), cv::Scalar(0));

	// Copy the both the warped right image and the original left image onto the blank canvas
	warped_img.copyTo(panorama(cv::Rect(0, 0, warped_img.cols, warped_img.rows)), warped_img != 0);
	img_left.copyTo(panorama(cv::Rect(0, 0, img_left.cols, img_left.rows)));

	return panorama;
}


int main(int argc, char** argv)
{
	using namespace std::chrono;

	cv::Mat img_left = cv::imread(argv[1], cv::IMREAD_ANYCOLOR);
	cv::Mat img_right = cv::imread(argv[2], cv::IMREAD_ANYCOLOR);

	if (argc != 3)
	{
		std::cout << "Supply exactly two images as input parameters." << std::endl;
		return -1;
	}
	if (!img_left.data || !img_right.data)
	{
		std::cout << "Could not read images! Check input parameters." << std::endl; 
		return -1;
	}

	const auto start = steady_clock::now();

	// Detect the keypoints using KAZE keypoint detector / descriptor extractor
	// Note: some default settings have been overridden to enhance decriptor quality
	const auto detector = cv::KAZE::create(true, true);

	std::vector<cv::KeyPoint> keypoints_left, keypoints_right;
	detector->detect(img_left, keypoints_left);
	detector->detect(img_right, keypoints_right);

	// Compute descriptors
	cv::Mat descriptors_left, descriptors_right;
	detector->detectAndCompute(img_left, img_left, keypoints_left, descriptors_left);
	detector->detectAndCompute(img_right, img_right, keypoints_right, descriptors_right);

	// Find matching descriptor vectors
	const auto good_matches = find_matching_descriptors(descriptors_left, descriptors_right);

	// Separate matching descriptors. One group for left image and one group for right image.
	std::vector<cv::Point2f> points_left, points_right;
	for each (const auto& good_match in good_matches)
	{
		points_left.push_back(keypoints_left[good_match.queryIdx].pt);
		points_right.push_back(keypoints_right[good_match.trainIdx].pt);
	}

	// Find homography matrix for transforming keypoint on the right image onto their counterparts on the left image
	const auto homography_matrix = cv::findHomography(points_right, points_left, cv::RANSAC);
	const auto panorama_size = calculate_panorama_size(img_left, img_right, homography_matrix);

	// Warp the right image to fit with the left image
	cv::Mat warped_img;
	cv::warpPerspective(img_right, warped_img, homography_matrix, panorama_size);

	// Stitch togethre a panorama based on unaltered left image and warped right image
	const auto panorama = create_panorama(img_left, warped_img, panorama_size);

	const auto end = steady_clock::now();

	std::cout << "Stitching operation took: " << duration_cast<milliseconds>(end - start).count() << " milliseconds." << std::endl;
	
	cv::imshow("Stitched panorama", panorama);
	cv::waitKey(0);

}


